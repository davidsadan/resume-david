// Easter Egg
var styles = {
    bold: 'color: purple; font-weight: bold;',
    normal: 'color: purple;'
};


// Scroller
function updateScroll(){
    var element = document.body;
    if((window.innerHeight + window.scrollY) >= document.body.offsetHeight) {
        element.scrollTop = element.scrollHeight;
    }
}


// Typewriter
var str = document.getElementById('code').innerHTML;
var i = 0;
var isTag;
var text;

document.getElementById('code').style.display = 'block';
(function type() {
    updateScroll();
    text = str.slice(0, ++i);

    if(text === str)
        return document.getElementById('code').innerHTML += '<span id="cursor">|</span>';

    document.getElementById('code').innerHTML = text;

    var char = text.slice(-1);

    if(char === '<')
        isTag = true;

    if(char === '>')
        isTag = false;

    if(isTag)
        return type();


    hljs.highlightBlock(document.getElementById('code'));

    if(text.length !== str.length -1)
        document.getElementById('code').innerHTML += '|';

    setTimeout(type, 20);
}());


// Didn't understand shit
var understood = false;

function explainPlease() {
    if(understood === false) {
        document.getElementById('code').style.display = 'none';
        document.getElementById('dummy').style.display = 'block';
        document.getElementById('explain').innerHTML = 'Ok I think I got it';
        understood = true;
    } else {
        document.body.style.background = '#23241f';
        document.getElementById('code').style.display = 'block';
        document.getElementById('dummy').style.display = 'none';
        document.getElementById('explain').innerHTML = 'I didn\'t understand sh*t';
        understood = false;
    }
}
